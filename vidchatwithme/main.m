//
//  main.m
//  OpenTokHelloWorld
//
//  Created by Jeff Swartz on 2/23/12.
//  Copyright (c) 2012 Coffee Bean Studios LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MyAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MyAppDelegate class]));
    }
}
