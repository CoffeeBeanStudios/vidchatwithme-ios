//
//  ChatServer.m
//  OpenTokHelloWorld
//
//  Created by Claude Sutterlin on 5/5/12.
//

#import "ChatServer.h"


@implementation ChatServer

- (ChatServer *) init{
    NSLog(@"Init chat server");
    _webSocket.delegate = nil;
    [_webSocket close];
    
    // connect the web socket to the server
    _webSocket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"ws://claude.nodester.com:80"]]];
    _webSocket.delegate = self;
    
    return self;
}

-(void) connect{
    [_webSocket open];
}

- (void) webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message{
    
    // grab the text from the message and convert to a dictionary
    NSData *jsonData = [message dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* json = [NSJSONSerialization 
                          JSONObjectWithData: jsonData
                          options:kNilOptions 
                          error:nil];
    
    NSString *type = [json objectForKey:@"type"];
    
    // set off our notifications depending on the message type
    if ([type isEqualToString:@"connected"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"connected" object:self];
    }
    else if ([type isEqualToString:@"session_result"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"session_result" object: self userInfo: json];
    }
    else if ([type isEqualToString:@"join_result"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"join_result" object: self userInfo: json];
    }
    else if ([type isEqualToString:@"new_user"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"new_user" object: self userInfo: json];
    }
}

-(bool) sendMsg: (NSMutableDictionary *) msg{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:msg 
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *output = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [_webSocket send: output];
    return YES;
}

-(void) getSession{
    NSMutableDictionary *msg = [[NSMutableDictionary alloc] init];
    [msg setObject:@"get_session" forKey:@"type"];
    
    [self sendMsg:msg];
}

@end
