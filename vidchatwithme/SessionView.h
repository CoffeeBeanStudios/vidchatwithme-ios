//
//  SessionView.h
//  OpenTokHelloWorld
//
//  Created by Claude Sutterlin on 5/5/12.
//

#import <UIKit/UIKit.h>
#import <Opentok/Opentok.h>

@interface SessionView : UIViewController <OTSessionDelegate, OTSubscriberDelegate, OTPublisherDelegate>{
    NSString *_sessionID;

    NSDictionary *userData;
    
    IBOutlet UIView *theirContainer;
    IBOutlet UIView *myContainer;
}

@property (nonatomic, retain) NSDictionary *userData;

- (void) setData: (NSDictionary *) userData;

- (void) connectSession: (NSString *)session withPubToken:(NSString *) pubToken andSubToken: (NSString *) subToken;
- (void)doPublish;
- (void)doSubscribe;
- (IBAction)disconnectClicked:(id)sender;
- (void) disconnect;
- (void) showAlert: (NSString *) message;

@end
