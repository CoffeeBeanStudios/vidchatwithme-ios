//
//  JoinView.h
//  OpenTokHelloWorld
//
//  Created by Claude Sutterlin on 4/25/12.
//

#import <UIKit/UIKit.h>
#import "ChatServer.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@class MailComposerViewController;

@interface JoinView : UIViewController<UIAlertViewDelegate, MFMailComposeViewControllerDelegate>{
    IBOutlet UITextField *codeBox;
    IBOutlet UIButton *emailBtn;
    
    IBOutlet UILabel *statusLbl;
    
    ChatServer *chatServer;
    
    NSDictionary *userData;
}

//properties
@property (nonatomic, retain) NSDictionary *userData;

- (void) setData: (NSDictionary *) data;

- (IBAction) emailClick:(id)sender;

- (void) newUserCallback:(NSNotification *) notification;
- (void) startSession;

@end
