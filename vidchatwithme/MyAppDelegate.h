//
//  AppDelegate.h
//
//  Created by Claude Sutterlin on 12/13/11.
//

#import <UIKit/UIKit.h>
#import "ChatServer.h"

@interface MyAppDelegate : UIResponder <UIApplicationDelegate>{
    ChatServer *chatServer;
    NSString *meetingParameter;
}

@property (strong, nonatomic) UIWindow *window;
@property (retain, nonatomic) ChatServer *chatServer;
@property (nonatomic, copy)   NSString* meetingParameter;

-(ChatServer *) getChatServer;

@end
