//
//  SessionView.m
//  OpenTokHelloWorld
//
//  Created by Claude Sutterlin on 5/5/12.
//

#import "SessionView.h"

@implementation SessionView{
    OTSession *_session;
    OTPublisher *_publisher;
    OTSubscriber *_subscriber;
}

static NSString* const kApiKey = @"413302";
static NSString* const kToken = @"devtoken";

@synthesize userData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) setData: (NSDictionary *) userData{
    self.userData = [NSDictionary dictionaryWithDictionary:userData];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _sessionID = [userData objectForKey:@"sessionid"];
    
    NSString *pubToken = [userData objectForKey:@"pubtoken"];
    NSString *subToken = [userData objectForKey:@"pubtoken"];
    
    [self connectSession:_sessionID withPubToken:pubToken andSubToken:subToken];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)disconnectClicked:(id)sender{
    [self disconnect];
}

- (void) connectSession: (NSString *)session withPubToken:(NSString *) pubToken andSubToken: (NSString *) subToken{
    
    _sessionID = session;
    
    _session = [[OTSession alloc] initWithSessionId:_sessionID
                                             delegate:self];
    
    [_session connectWithApiKey:kApiKey token:pubToken];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationPortrait) || (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown));
}

- (void) disconnect{
    [_session disconnect];
    
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - OpenTok methods


- (void)doPublish
{
    _publisher = [[OTPublisher alloc] initWithDelegate:self];
    [_publisher setName:[[UIDevice currentDevice] name]];
    [_session publish:_publisher];
    [self.view addSubview:_publisher.view];
    [_publisher.view setFrame:CGRectMake(0, 0, 128, 96)];
    [self.view bringSubviewToFront:_publisher.view];
}

- (void)doSubscribe{
    for (NSString* streamId in _session.streams) {
        OTStream* stream = [_session.streams valueForKey:streamId];
        if (![stream.connection.connectionId isEqualToString: _session.connection.connectionId]) {
            _subscriber = [[OTSubscriber alloc] initWithStream:stream delegate:self];
            break;
        }
    }  
}

- (void)sessionDidConnect:(OTSession*)session
{
    NSLog(@"sessionDidConnect (%@)", session.sessionId);
    [self doSubscribe];
    [self doPublish];
}

- (void)sessionDidDisconnect:(OTSession*)session
{

}


- (void)session:(OTSession*)mySession didReceiveStream:(OTStream*)stream
{
    NSLog(@"session didReceiveStream (%@)", stream.streamId);
    
    //only care about incoming connections for the partner
    [self doSubscribe];
}

- (void)session:(OTSession*)session didDropStream:(OTStream*)stream{
    NSLog(@"session didDropStream (%@)", stream.streamId);
    NSLog(@"_subscriber.stream.streamId (%@)", _subscriber.stream.streamId);
    
    //only care about incoming connections for the partner
    if ([session.sessionId isEqualToString:_sessionID]){
        _subscriber = nil;
        [self doSubscribe];
    }
}

- (void)subscriberDidConnectToStream:(OTSubscriber*)subscriber
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    CGFloat viewHeight = screenHeight;
    CGFloat viewWidth = (screenHeight*1.5); //maintain proportions
    CGFloat viewLeft = (((viewWidth-screenWidth)/2)*-1); //shift left half the distance to center it;
    CGFloat viewTop = 0;
    
    NSLog(@"subscriberDidConnectToStream %@", subscriber.stream.connection.connectionId);
    [subscriber.view setFrame:CGRectMake(viewLeft, viewTop, viewWidth, viewHeight)];
    [self.view addSubview:subscriber.view];
    [self.view sendSubviewToBack:subscriber.view];
    
}

- (void)publisher:(OTPublisher*)publisher didFailWithError:(NSError*) error {
    NSLog(@"publisher didFailWithError %@", error);
    [self showAlert:[NSString stringWithFormat:@"There was an error publishing."]];
}

- (void)subscriber:(OTSubscriber*)subscriber didFailWithError:(NSError*)error
{
    NSLog(@"subscriber %@ didFailWithError %@", subscriber.stream.streamId, error);
    [self showAlert:[NSString stringWithFormat:@"There was an error subscribing to stream %@", subscriber.stream.streamId]];
}

- (void)session:(OTSession*)session didFailWithError:(NSError*)error {
    NSLog(@"sessionDidFail");
    [self showAlert:[NSString stringWithFormat:@"There was an error connecting to session %@", session.sessionId]];
}

- (void) showAlert: (NSString *) message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Meeting Alert" message:message delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles: nil];
    [alert show];
}

@end
