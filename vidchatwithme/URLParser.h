//
//  URLParser.h
//  VidChatWithMe
//
//  Created by Claude Sutterlin on 5/19/12.
//  Copyright (c) 2012 Coffee Bean Studios LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLParser : NSObject{
    NSArray *variables;
}
 
@property (nonatomic, retain) NSArray *variables;
 
- (id)initWithURLString:(NSString *)url;
- (NSString *)valueForVariable:(NSString *)varName;
 
@end
