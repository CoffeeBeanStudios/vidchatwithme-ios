//
//  ChatServer.h
//  OpenTokHelloWorld
//
//  Created by Claude Sutterlin on 5/5/12.
//

#import <Foundation/Foundation.h>
#import "SRWebSocket.h"

@interface ChatServer : NSObject<SRWebSocketDelegate>{
    SRWebSocket *_webSocket;
    
}

- (void) connect;
- (bool) sendMsg: (NSMutableDictionary *) msg;
- (void) getSession;



@end
