//
//  JoinView.m
//  OpenTokHelloWorld
//
//  Created by Claude Sutterlin on 4/25/12.
//

#import "JoinView.h"
#import "UserNameView.h"
#import "MyAppDelegate.h"
#import "SessionView.h"
#import "MailComposerViewController.h"

@implementation JoinView{
    bool inSession;
}

@synthesize userData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle


- (IBAction) emailClick:(id)sender{
    // Create a new email message
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        
        // create the email information
        [mailCont setSubject:@"vidChatWith.me Invitation"];
        NSString *key = codeBox.text;
        NSString *body = [NSString stringWithFormat:@"<body><p>You have been invited to join a video chat meeting!</p><br/><p><a href=\"http://www.vidchatwith.me/?meeting=%@\">Click here to join this meeting from your PC or Mac</a></p><br/><p><a href='vidchatwithme://?meeting=%@'>Click here to join this meeting from your iPhone or iPad</a></p></body>", key, key];
                          
        [mailCont setMessageBody:body isHTML:YES];
        
        [self presentModalViewController:mailCont animated:YES];
    }
}

// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissModalViewControllerAnimated:YES];
}

- (void) setData: (NSDictionary *) data{
    //[codeBox setText:meetingCode];
    self.userData = [NSDictionary dictionaryWithDictionary: data];
}

- (void) inviteOutCallback:(NSNotification *) notification{
    NSDictionary *json = notification.userInfo;
    [statusLbl setText: [json valueForKey:@"message"]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:@"Your meeting"];
    inSession = false;
    
    // grab the server from the app delegate
    MyAppDelegate *appDel = (MyAppDelegate*)[[UIApplication sharedApplication] delegate];
    chatServer = appDel.chatServer;
    
    //set up my call backs I'm interested in here
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(newUserCallback:) 
                                                 name:@"new_user"
                                               object:nil];
    
    // if our meeting already has people in it then we'll go ahead and join up
    [codeBox setText:[userData objectForKey:@"key"]];

    NSString *users_str = [userData objectForKey: @"members"];
    int users_int = [users_str intValue];
    
    if (users_int > 1){
        [self startSession];
    }
}

- (void) newUserCallback:(NSNotification *) notification{
    if (!inSession){
        [self startSession];
    }
}

- (void) startSession{
    SessionView *sessionView = [self.storyboard instantiateViewControllerWithIdentifier:@"SessionView"];
    [sessionView setData: userData];
    [self.navigationController presentModalViewController: sessionView animated:YES]; 
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationPortrait) || (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown));
}

@end
