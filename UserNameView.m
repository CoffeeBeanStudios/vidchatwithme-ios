//
//  UserNameView.m
//  OpenTokHelloWorld
//
//  Created by Claude Sutterlin on 4/24/12.
//

#import "UserNameView.h"
#import "MyAppDelegate.h"
#import "JoinView.h"

@implementation UserNameView

@synthesize loginBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setTitle:@"Login"];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // grab the chat server from the delegate
    MyAppDelegate *appDel = (MyAppDelegate*)[[UIApplication sharedApplication] delegate];
    chatServer = appDel.chatServer;

    //set up my call backs I'm interested in here
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(connectedCallback:) 
                                                 name:@"connected"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sessionCallback:) 
                                                 name:@"session_result"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(joinCallback:) 
                                                 name:@"join_result"
                                               object:nil];
    
    // set the result and wait for the connections
    [resultLbl setText: @"Opening Connection..."];
    [loginBtn setEnabled:NO];
    
    //initiate the connection
    [chatServer connect];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationPortrait) || (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown));
}

- (IBAction)createNewClicked:(id)sender{
    NSMutableDictionary *msg = [[NSMutableDictionary alloc] init];
    [msg setObject:@"new_session" forKey:@"type"];
    
    [chatServer sendMsg: msg];
}

- (IBAction) joinExistingClicked:(id)sender{
    NSString *subkey = [subkeyBox text];
        
    // attempt to join an existing meeting
    NSMutableDictionary *msg = [[NSMutableDictionary alloc] init];
    [msg setObject:@"join_session" forKey:@"type"];
    [msg setObject: subkey forKey:@"key"];
    
    // send the request
    [chatServer sendMsg: msg];
}

-(void) sessionCallback:(NSNotification *) notification{
    
    NSDictionary *userInfo = notification.userInfo;

    [resultLbl setText:@"Getting meeting..."];
    
    // create the join view
    JoinView *joinView = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinView"];
    [joinView setData: userInfo];
    [self.navigationController pushViewController: joinView animated:YES]; 
}

-(void) joinCallback:(NSNotification *)notification{
    // this is mostly just informational
    NSDictionary *userInfo = notification.userInfo;
    [resultLbl setText: [userInfo objectForKey:@"message"]];
}

- (void)connectedCallback:(id)sender{
    // if we got this back then we were able to contact the server
    [resultLbl setText:@"Connected"];
    [loginBtn setEnabled:YES];
    
    //now that we're connected, see if we were passed a meeting as a parameter
    //  if we were, go ahead and join that meeting
    MyAppDelegate *appDel = (MyAppDelegate*)[[UIApplication sharedApplication] delegate];
    if (![appDel.meetingParameter isEqualToString:@""]){
        [subkeyBox setText:appDel.meetingParameter];
        [self joinExistingClicked:nil];
    }
    
}



@end
