//
//  UserNameView.h
//  OpenTokHelloWorld
//
//  Created by Claude Sutterlin on 4/24/12.
//

#import <UIKit/UIKit.h>
#import "ChatServer.h"

@interface UserNameView : UIViewController{
    IBOutlet UIButton *joinBtn;
    IBOutlet UILabel *resultLbl;
    
    IBOutlet UITextField *subkeyBox;
    
    ChatServer *chatServer;
}

@property (nonatomic, retain) UIButton *loginBtn;

- (IBAction)createNewClicked:(id)sender;
- (IBAction)joinExistingClicked:(id)sender;


- (void) connectedCallback:(id) sender;
- (void) sessionCallback:(NSNotification *) notification;
- (void) joinCallback:(NSNotification *) notification;



@end
